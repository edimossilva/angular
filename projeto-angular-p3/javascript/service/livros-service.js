angular.module('livrex')
.service('livrosService', function() {

  this.livros = [
    new Livro ('Caixa de passaros',
           'Imagine viver sem abrir os olhos ',
           'https://images-submarino.b2w.io/produtos/01/00/item/121567/8/121567855_1GG.jpg'
          ),
    new Livro ('Biblioteca de almas',
           'Livro 3 do orfanato',
           'https://images-submarino.b2w.io/produtos/01/00/item/128558/2/128558290_1GG.jpg'
          ),
    new Livro ('Harry Potter',
           'E a criança amaldiçoada',
           'https://images-submarino.b2w.io/produtos/01/00/item/128892/0/128892010_1GG.jpg'
          )
  ]
  
  this.addLivro = function(livro) {
	   this.livros.push(livro)
  }
  
  this.removeLivro = function(livro) {
    	var index = livros.indexOf(livro);
    	this.livros.remove(index)
  }

});