angular.module('minhasDiretivas', [])
.directive('meuPainel', function() { // o nome tem que ser camelCase
        var ddo = {};
        ddo.restrict = "E"; // A = atributo, E = Elemento
        ddo.scope = {
            livros: '=livros'
        };
        ddo.template =     '<div class="panel panel-default col-md-3" ng-repeat= "livro in livros">'
			            +   '   <div class="panel-heading">'
			            +   '        <h3 class="panel-title text-center">{{livro.toString()}}</h3>'
			            +   '   </div>'
			            +   '   <div class="panel-body">'
			            +   '   <img class="img-responsive center-block" src="{{livro.imagemURL}}">'
			            +   '   </div>'
			            +   '</div>'
        return ddo;
    });