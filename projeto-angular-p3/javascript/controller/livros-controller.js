angular.module('livrex').controller('LivrosController',function($scope, livrosService){
	
	$scope.livro = new Livro()
	$scope.livros = livrosService.livros

	$scope.adicionarLivro = function (livro){
		livrosService.addLivro(livro)
		$scope.livro = new Livro()
	}

});