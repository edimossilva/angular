class Livro {
	constructor(titulo, descricao, imagemURL){
		this._titulo = titulo
		this._descricao = descricao
		this._imagemURL = imagemURL
	}

	get titulo(){
	    return this._titulo
	}
	set titulo(titulo){
    	this._titulo = titulo
  	}
	
	get descricao(){
	    return this._descricao
	}
	set descricao(descricao){
    	this._descricao = descricao
  	}
	

	get imagemURL(){
	    return this._imagemURL
	}
	
	set imagemURL(imagemURL){
    	this._imagemURL = imagemURL
  	}
	
	toString(){
		return this.titulo + " - " + this.descricao
	}
}